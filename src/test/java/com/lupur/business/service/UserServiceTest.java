package com.lupur.business.service;

import com.lupur.api.UserRestTemplate;
import com.lupur.business.dto.RoleDTO;
import com.lupur.business.dto.UserDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestClientException;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by Razvan Alexandru Lupu on 21-Jul-17.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration()
public class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRestTemplate userRestTemplate;

    @Mock
    private PasswordEncoder mockPasswordEncoder;

    private PasswordEncoder passwordEncoder;

    @Before
    public void setUp() throws Exception {
        passwordEncoder = new BCryptPasswordEncoder();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getUsers_success() throws Exception {
        UserDTO user = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", Date.valueOf("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());
        UserDTO user2 = new UserDTO(2, "John", "Branch", "johnbranch", "password", "johnbranch@gmail.com", Date.valueOf("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());
        List<UserDTO> userDTOList = new ArrayList<>();
        userDTOList.add(user);
        userDTOList.add(user2);

        when(userRestTemplate.getUsers()).thenReturn(userDTOList);
        assertEquals(userDTOList, userService.getUsers());
        verify(userRestTemplate, times(1)).getUsers();
    }

    @Test
    public void getUser_success() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", Date.valueOf("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());
        when(userRestTemplate.getUser(userDTO.getId())).thenReturn(userDTO);

        assertEquals(userDTO, userService.getUser(userDTO.getId()));
        verify(userRestTemplate, times(1)).getUser(userDTO.getId());
    }

    @Test(expected = RestClientException.class)
    public void getUser_notFound() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", Date.valueOf("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());
        when(userRestTemplate.getUser(userDTO.getId())).thenThrow(new RestClientException(""));
        //TODO check error response
        userService.getUser(userDTO.getId());
        verify(userRestTemplate, times(1)).getUser(userDTO.getId());
    }

    @Test
    public void createUser_success() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", Date.valueOf("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());

        when(userRestTemplate.createUser(userDTO)).thenReturn(userDTO);

        String encodedPassword = passwordEncoder.encode(userDTO.getPassword());
        when(mockPasswordEncoder.encode(userDTO.getPassword())).thenReturn(encodedPassword);
        when(mockPasswordEncoder.matches(encodedPassword, userDTO.getPassword())).thenReturn(true);
        assertEquals(userDTO, userService.createUser(userDTO));
        verify(userRestTemplate, times(1)).createUser(userDTO);
    }

    @Test(expected = RestClientException.class)
    public void createUser_failure() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", Date.valueOf("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());

        String encodedPassword = passwordEncoder.encode(userDTO.getPassword());
        when(mockPasswordEncoder.encode(userDTO.getPassword())).thenReturn(encodedPassword);
        when(mockPasswordEncoder.matches(encodedPassword, userDTO.getPassword())).thenReturn(true);
        when(userRestTemplate.createUser(userDTO)).thenThrow(new RestClientException(""));
        //TODO check error response
        assertEquals(userDTO, userService.createUser(userDTO));
        verify(userRestTemplate, times(1)).createUser(userDTO);
    }

    @Test
    public void updateUser_success() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", Date.valueOf("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());

        when(userRestTemplate.updateUser(userDTO.getId(), userDTO)).thenReturn(userDTO);
        when(userRestTemplate.getUser(userDTO.getId())).thenReturn(userDTO);

        assertEquals(userDTO, userService.updateUser(userDTO.getId(), userDTO));
        verify(userRestTemplate, times(1)).updateUser(userDTO.getId(), userDTO);
    }

    @Test(expected = RestClientException.class)
    public void updateUser_notFound() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", Date.valueOf("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());

        when(userRestTemplate.updateUser(userDTO.getId(), userDTO)).thenReturn(userDTO);
        when(userRestTemplate.getUser(userDTO.getId())).thenThrow(new RestClientException(""));
        //TODO check error response
        assertEquals(userDTO, userService.updateUser(userDTO.getId(), userDTO));
        verify(userRestTemplate, times(0)).updateUser(userDTO.getId(), userDTO);
        verify(userRestTemplate, times(1)).getUser(userDTO.getId());
    }

    @Test(expected = RestClientException.class)
    public void updateUser_validationError() throws Exception {
        UserDTO userDTO = new UserDTO(1, "L", "R", "lupurazvan", "password", "lupurazvan@gmail.com", Date.valueOf("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());

        when(userRestTemplate.updateUser(userDTO.getId(), userDTO)).thenReturn(userDTO);
        when(userRestTemplate.getUser(userDTO.getId())).thenThrow(new RestClientException(""));
        //TODO check error response
        assertEquals(userDTO, userService.updateUser(userDTO.getId(), userDTO));
        verify(userRestTemplate, times(0)).updateUser(userDTO.getId(), userDTO);
        verify(userRestTemplate, times(1)).getUser(userDTO.getId());
    }

    @Test
    public void deleteUser_success() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", Date.valueOf("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());

        when(userRestTemplate.deleteUser(userDTO.getId())).thenReturn(userDTO);
        when(userRestTemplate.getUser(userDTO.getId())).thenReturn(userDTO);

        assertEquals(userDTO, userService.deleteUser(userDTO.getId()));
        verify(userRestTemplate, times(1)).deleteUser(userDTO.getId());
    }

    @Test(expected = RestClientException.class)
    public void deleteUser_notFound() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", Date.valueOf("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());

        when(userRestTemplate.deleteUser(userDTO.getId())).thenThrow(new RestClientException(""));
        //TODO check error response
        assertEquals(userDTO, userService.deleteUser(userDTO.getId()));
        verify(userRestTemplate, times(1)).deleteUser(userDTO.getId());
    }

}