package com.lupur.api;

import com.lupur.business.dto.RoleDTO;
import com.lupur.business.dto.UserDTO;
import org.apache.tomcat.util.codec.binary.Base64;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by Razvan Alexandru Lupu on 26-Jul-17.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class AuthenticationRestTemplateTest {

    @InjectMocks
    private AuthenticationRestTemplate authenticationRestTemplate;

    @Mock
    private RestTemplate restTemplate;

    private String REST_API_URL;

    private String REST_USERNAME;

    private String REST_PASSWORD;


    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void findByUsername_success() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());

        HttpHeaders httpHeaders = new HttpHeaders();
        addAuthorizationHeader(httpHeaders);
        HttpEntity<String> request = new HttpEntity<>(httpHeaders);
        ResponseEntity<UserDTO> response = (ResponseEntity<UserDTO>) mock(ResponseEntity.class);
        when(response.getBody()).thenReturn(userDTO);
        when(restTemplate.exchange(REST_API_URL + "/users?username=" + userDTO.getUsername(), HttpMethod.GET, request, UserDTO.class)).thenReturn(response);

        assertEquals(userDTO, authenticationRestTemplate.findByUsername(userDTO.getUsername()));

        verify(restTemplate, times(1)).exchange(REST_API_URL + "/users?username=" + userDTO.getUsername(), HttpMethod.GET, request, UserDTO.class);
    }

    @Test(expected = RestClientException.class)
    public void findByUsername_notFound() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());

        HttpHeaders httpHeaders = new HttpHeaders();
        addAuthorizationHeader(httpHeaders);
        HttpEntity<String> request = new HttpEntity<>(httpHeaders);
        ResponseEntity<UserDTO> response = (ResponseEntity<UserDTO>) mock(ResponseEntity.class);
        when(response.getBody()).thenReturn(userDTO);
        when(restTemplate.exchange(REST_API_URL + "/users?username=" + userDTO.getUsername(), HttpMethod.GET, request, UserDTO.class)).thenThrow(new RestClientException(""));

        assertEquals(userDTO, authenticationRestTemplate.findByUsername(userDTO.getUsername()));

        verify(restTemplate, times(1)).exchange(REST_API_URL + "/users?username=" + userDTO.getUsername(), HttpMethod.GET, request, UserDTO.class);
    }

    private void addAuthorizationHeader(HttpHeaders httpHeaders) {
        String plainCredentials = REST_USERNAME + ":" + REST_PASSWORD;
        String base64Credentials = Base64.encodeBase64String(plainCredentials.getBytes());
        httpHeaders.add("Authorization", "Basic " + base64Credentials);
    }

}