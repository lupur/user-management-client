package com.lupur.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lupur.business.dto.RoleDTO;
import com.lupur.business.dto.UserDTO;
import org.apache.tomcat.util.codec.binary.Base64;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by Razvan Alexandru Lupu on 24-Jul-17.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class UserRestTemplateTest {

    private String REST_API_URL;

    private String REST_USERNAME;

    private String REST_PASSWORD;


    @InjectMocks
    private UserRestTemplate userRestTemplate;

    @Mock
    private RestTemplate restTemplate;

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getUser() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());

        HttpHeaders httpHeaders = new HttpHeaders();
        addAuthorizationHeader(httpHeaders);
        HttpEntity<String> request = new HttpEntity<>(httpHeaders);
        ResponseEntity<UserDTO> response = (ResponseEntity<UserDTO>) mock(ResponseEntity.class);
        when(response.getBody()).thenReturn(userDTO);
        when(restTemplate.exchange(REST_API_URL + "/users/" + userDTO.getId(), HttpMethod.GET, request, UserDTO.class)).thenReturn(response);

        assertEquals(userDTO, userRestTemplate.getUser(userDTO.getId()));
        verify(restTemplate, times(1)).exchange(REST_API_URL + "/users/" + userDTO.getId(), HttpMethod.GET, request, UserDTO.class);
    }

    @Test(expected = RestClientException.class)
    public void getUser_NotFound() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());

        HttpHeaders httpHeaders = new HttpHeaders();
        addAuthorizationHeader(httpHeaders);
        HttpEntity<String> request = new HttpEntity<>(httpHeaders);
        ResponseEntity<UserDTO> response = (ResponseEntity<UserDTO>) mock(ResponseEntity.class);
        when(response.getBody()).thenReturn(userDTO);
        when(restTemplate.exchange(REST_API_URL + "/users/" + userDTO.getId(), HttpMethod.GET, request, UserDTO.class)).thenThrow(new RestClientException(""));

        assertEquals(userDTO, userRestTemplate.getUser(userDTO.getId()));
        verify(restTemplate, times(1)).exchange(REST_API_URL + "/users/" + userDTO.getId(), HttpMethod.GET, request, UserDTO.class);
    }

    @Test
    public void getUsersByRole() throws Exception {
        String roleName = "ADMIN";
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());
        UserDTO userDTO2 = new UserDTO(2, "John", "Branch", "johnbranch", "password", "johnbranch@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());
        List<UserDTO> userDTOList = new ArrayList<>();
        userDTOList.add(userDTO);
        userDTOList.add(userDTO2);

        HttpHeaders httpHeaders = new HttpHeaders();
        addAuthorizationHeader(httpHeaders);
        HttpEntity<String> request = new HttpEntity<>(httpHeaders);
        ResponseEntity<UserDTO[]> response = (ResponseEntity<UserDTO[]>) mock(ResponseEntity.class);
        when(response.getBody()).thenReturn(userDTOList.toArray(new UserDTO[userDTOList.size()]));
        when(restTemplate.exchange(REST_API_URL + "/users?role=" + roleName, HttpMethod.GET, request, UserDTO[].class)).thenReturn(response);

        assertEquals(userDTOList, userRestTemplate.getUsersByRole(roleName));

        verify(restTemplate, times(1)).exchange(REST_API_URL + "/users?role=" + roleName, HttpMethod.GET, request, UserDTO[].class);
    }

    @Test
    public void getUsers() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());
        UserDTO userDTO2 = new UserDTO(2, "John", "Branch", "johnbranch", "password", "johnbranch@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());
        List<UserDTO> userDTOList = new ArrayList<>();
        userDTOList.add(userDTO);
        userDTOList.add(userDTO2);

        HttpHeaders httpHeaders = new HttpHeaders();
        addAuthorizationHeader(httpHeaders);
        HttpEntity<String> request = new HttpEntity<>(httpHeaders);
        ResponseEntity<UserDTO[]> response = (ResponseEntity<UserDTO[]>) mock(ResponseEntity.class);
        when(response.getBody()).thenReturn(userDTOList.toArray(new UserDTO[userDTOList.size()]));
        when(restTemplate.exchange(REST_API_URL + "/users", HttpMethod.GET, request, UserDTO[].class)).thenReturn(response);

        assertEquals(userDTOList, userRestTemplate.getUsers());

        verify(restTemplate, times(1)).exchange(REST_API_URL + "/users", HttpMethod.GET, request, UserDTO[].class);
    }

    @Test
    public void createUser_success() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());

        HttpHeaders httpHeaders = new HttpHeaders();
        addAuthorizationHeader(httpHeaders);
        HttpEntity<UserDTO> request = new HttpEntity<>(userDTO, httpHeaders);
        ResponseEntity<UserDTO> response = (ResponseEntity<UserDTO>) mock(ResponseEntity.class);
        when(response.getBody()).thenReturn(userDTO);
        when(restTemplate.exchange(REST_API_URL + "/users", HttpMethod.POST, request, UserDTO.class)).thenReturn(response);

        assertEquals(userDTO, userRestTemplate.createUser(userDTO));
        verify(restTemplate, times(1)).exchange(REST_API_URL + "/users", HttpMethod.POST, request, UserDTO.class);
    }

    @Test(expected = RestClientException.class)
    public void createUser_failure() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());

        HttpHeaders httpHeaders = new HttpHeaders();
        addAuthorizationHeader(httpHeaders);
        HttpEntity<UserDTO> request = new HttpEntity<>(userDTO, httpHeaders);
        ResponseEntity<UserDTO> response = (ResponseEntity<UserDTO>) mock(ResponseEntity.class);
        when(response.getBody()).thenReturn(userDTO);
        when(restTemplate.exchange(REST_API_URL + "/users", HttpMethod.POST, request, UserDTO.class)).thenThrow(new RestClientException(""));

        assertEquals(userDTO, userRestTemplate.createUser(userDTO));
        verify(restTemplate, times(1)).exchange(REST_API_URL + "/users", HttpMethod.POST, request, UserDTO.class);
    }

    @Test
    public void deleteUser_success() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());

        HttpHeaders httpHeaders = new HttpHeaders();
        addAuthorizationHeader(httpHeaders);
        HttpEntity<String> request = new HttpEntity<>(httpHeaders);
        ResponseEntity<UserDTO> response = (ResponseEntity<UserDTO>) mock(ResponseEntity.class);
        when(response.getBody()).thenReturn(userDTO);
        when(restTemplate.exchange(REST_API_URL + "/users/" + userDTO.getId(), HttpMethod.DELETE, request, UserDTO.class)).thenReturn(response);

        assertEquals(userDTO, userRestTemplate.deleteUser(userDTO.getId()));
        verify(restTemplate, times(1)).exchange(REST_API_URL + "/users/" + userDTO.getId(), HttpMethod.DELETE, request, UserDTO.class);
    }

    @Test(expected = RestClientException.class)
    public void deleteUser_failure() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());

        HttpHeaders httpHeaders = new HttpHeaders();
        addAuthorizationHeader(httpHeaders);
        HttpEntity<String> request = new HttpEntity<>(httpHeaders);
        ResponseEntity<UserDTO> response = (ResponseEntity<UserDTO>) mock(ResponseEntity.class);
        when(response.getBody()).thenReturn(userDTO);
        when(restTemplate.exchange(REST_API_URL + "/users/" + userDTO.getId(), HttpMethod.DELETE, request, UserDTO.class)).thenThrow(new RestClientException(""));

        assertEquals(userDTO, userRestTemplate.deleteUser(userDTO.getId()));
        verify(restTemplate, times(1)).exchange(REST_API_URL + "/users/" + userDTO.getId(), HttpMethod.DELETE, request, UserDTO.class);
    }

    @Test
    public void updateUser_success() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());

        HttpHeaders httpHeaders = new HttpHeaders();
        addAuthorizationHeader(httpHeaders);
        HttpEntity<UserDTO> request = new HttpEntity<>(userDTO, httpHeaders);
        ResponseEntity<UserDTO> response = (ResponseEntity<UserDTO>) mock(ResponseEntity.class);
        when(response.getBody()).thenReturn(userDTO);
        when(restTemplate.exchange(REST_API_URL + "/users/" + userDTO.getId(), HttpMethod.PUT, request, UserDTO.class)).thenReturn(response);

        assertEquals(userDTO, userRestTemplate.updateUser(userDTO.getId(), userDTO));
        verify(restTemplate, times(1)).exchange(REST_API_URL + "/users/" + userDTO.getId(), HttpMethod.PUT, request, UserDTO.class);
    }

    @Test(expected = RestClientException.class)
    public void updateUser_failure() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());

        HttpHeaders httpHeaders = new HttpHeaders();
        addAuthorizationHeader(httpHeaders);
        HttpEntity<UserDTO> request = new HttpEntity<>(userDTO, httpHeaders);
        ResponseEntity<UserDTO> response = (ResponseEntity<UserDTO>) mock(ResponseEntity.class);
        when(response.getBody()).thenReturn(userDTO);
        when(restTemplate.exchange(REST_API_URL + "/users/" + userDTO.getId(), HttpMethod.PUT, request, UserDTO.class)).thenThrow(new RestClientException(""));

        assertEquals(userDTO, userRestTemplate.updateUser(userDTO.getId(), userDTO));
        verify(restTemplate, times(1)).exchange(REST_API_URL + "/users/" + userDTO.getId(), HttpMethod.PUT, request, UserDTO.class);
    }

    private void addAuthorizationHeader(HttpHeaders httpHeaders) {
        String plainCredentials = REST_USERNAME + ":" + REST_PASSWORD;
        String base64Credentials = Base64.encodeBase64String(plainCredentials.getBytes());
        httpHeaders.add("Authorization", "Basic " + base64Credentials);
    }
}