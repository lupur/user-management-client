package com.lupur.presentation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lupur.business.dto.RoleDTO;
import com.lupur.business.dto.UserDTO;
import com.lupur.business.service.UserService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestClientException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Created by Razvan Alexandru Lupu on 20-Jul-17.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(value = UserController.class, secure = false)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getUsers_success() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());
        UserDTO userDTO2 = new UserDTO(2, "John", "Branch", "johnbranch", "password", "johnbranch@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());
        List<UserDTO> userDTOList = new ArrayList<>();
        userDTOList.add(userDTO);
        userDTOList.add(userDTO2);
        when(userService.getUsers()).thenReturn(userDTOList);

        mockMvc.perform(get("/users/"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(model().attribute("userDTOList", userDTOList));
        verify(userService, times(1)).getUsers();
    }

    @Test
    public void getUser_success() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());
        List<UserDTO> userDTOList = new ArrayList<>();
        userDTOList.add(userDTO);
        when(userService.getUser(userDTO.getId())).thenReturn(userDTO);

        mockMvc.perform(get("/users/" + userDTO.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(model().attribute("userDTOList", userDTOList));
        verify(userService, times(1)).getUser(userDTO.getId());
    }

    @Test()
    public void getUser_notFound() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());
        when(userService.getUser(userDTO.getId())).thenThrow(new RestClientException(""));

        mockMvc.perform(get("/users/" + userDTO.getId()))
                .andExpect(status().isInternalServerError())
                .andExpect(content().contentType("text/html;charset=UTF-8"));
        verify(userService, times(1)).getUser(userDTO.getId());
    }

    @Test
    public void createUserGet() throws Exception {
        mockMvc.perform(get("/users/create"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(model().attribute("userDTO", new UserDTO()));
    }

    @Test
    public void createUserPost_success() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", null);
        when(userService.createUser(userDTO)).thenReturn(userDTO);

        mockMvc.perform(post("/users/create")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("id", userDTO.getId().toString())
                .param("firstName", userDTO.getFirstName())
                .param("lastName", userDTO.getLastName())
                .param("username", userDTO.getUsername())
                .param("password", userDTO.getPassword())
                .param("email", userDTO.getEmail())
                .param("dateOfBirth", "1995-07-15")
                .param("phone", userDTO.getPhone())
                .param("address", userDTO.getAddress()))
                .andExpect(status().isFound());
        verify(userService, times(1)).createUser(userDTO);
    }

    @Test
    public void createUserPost_failure() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());
        when(userService.getUser(userDTO.getId())).thenThrow(new RestClientException(""));

        mockMvc.perform(get("/users/create"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(model().attribute("userDTO", new UserDTO()));
    }

    @Test
    public void editUserGet_success() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());
        when(userService.getUser(userDTO.getId())).thenReturn(userDTO);

        mockMvc.perform(get("/users/edit/" + userDTO.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(model().attribute("userDTO", userDTO));
        verify(userService, times(1)).getUser(userDTO.getId());
    }

    @Test
    public void editUserGet_notFound() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());
        when(userService.getUser(userDTO.getId())).thenThrow(new RestClientException(""));

        mockMvc.perform(get("/users/edit/" + userDTO.getId()))
                .andExpect(status().isInternalServerError())
                .andExpect(content().contentType("text/html;charset=UTF-8"));
        verify(userService, times(1)).getUser(userDTO.getId());
    }

    @Test
    public void editUserPost_success() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", null);
        when(userService.updateUser(userDTO.getId(), userDTO)).thenReturn(userDTO);

        mockMvc.perform(post("/users/edit/" + userDTO.getId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("id", userDTO.getId().toString())
                .param("firstName", userDTO.getFirstName())
                .param("lastName", userDTO.getLastName())
                .param("username", userDTO.getUsername())
                .param("password", userDTO.getPassword())
                .param("email", userDTO.getEmail())
                .param("dateOfBirth", "1995-07-15")
                .param("phone", userDTO.getPhone())
                .param("address", userDTO.getAddress()))
                .andExpect(status().isFound());
        verify(userService, times(1)).updateUser(userDTO.getId(), userDTO);
    }

    @Test
    public void editUserPost_notFound() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", null);
        when(userService.updateUser(userDTO.getId(), userDTO)).thenThrow(new RestClientException(""));

        mockMvc.perform(post("/users/edit/" + userDTO.getId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("id", userDTO.getId().toString())
                .param("firstName", userDTO.getFirstName())
                .param("lastName", userDTO.getLastName())
                .param("username", userDTO.getUsername())
                .param("password", userDTO.getPassword())
                .param("email", userDTO.getEmail())
                .param("dateOfBirth", "1995-07-15")
                .param("phone", userDTO.getPhone())
                .param("address", userDTO.getAddress()))
                .andExpect(status().isInternalServerError())
                .andExpect(content().contentType("text/html;charset=UTF-8"));
        verify(userService, times(1)).updateUser(userDTO.getId(), userDTO);
    }

    @Test
    public void deleteUser_success() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());
        when(userService.deleteUser(userDTO.getId())).thenReturn(userDTO);

        mockMvc.perform(get("/users/delete/" + userDTO.getId()))
                .andExpect(status().isFound());
    }

    @Test
    public void deleteUser_notFound() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());
        when(userService.deleteUser(userDTO.getId())).thenThrow(new RestClientException(""));

        mockMvc.perform(get("/users/delete/" + userDTO.getId()))
                .andExpect(status().isInternalServerError())
                .andExpect(content().contentType("text/html;charset=UTF-8"));
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}