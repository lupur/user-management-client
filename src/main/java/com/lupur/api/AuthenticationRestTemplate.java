package com.lupur.api;

import com.lupur.business.dto.UserDTO;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;


/**
 * Created by Razvan Alexandru Lupu on 19-Jul-17.
 */

@Component("authenticationRestTemplate")
public class AuthenticationRestTemplate {


    @Value("${rest.server.url}")
    private String AUTH_API_URL;

    @Value("${rest.server.username}")
    private String AUTH_USERNAME;

    @Value("${rest.server.password}")
    private String AUTH_PASSWORD;

    @Autowired
    private RestTemplate restTemplate;

    public UserDTO findByUsername(String username) throws RestClientException {
        HttpHeaders httpHeaders = new HttpHeaders();
        addAuthorizationHeader(httpHeaders);
        HttpEntity<String> request = new HttpEntity<>(httpHeaders);
        ResponseEntity<UserDTO> response = restTemplate.exchange(AUTH_API_URL + "/users?username=" + username, HttpMethod.GET, request, UserDTO.class);
        return response.getBody();
    }

    private void addAuthorizationHeader(HttpHeaders httpHeaders) {
        String plainCredentials = AUTH_USERNAME + ":" + AUTH_PASSWORD;
        String base64Credentials = Base64.encodeBase64String(plainCredentials.getBytes());
        httpHeaders.add("Authorization", "Basic " + base64Credentials);
    }
}
