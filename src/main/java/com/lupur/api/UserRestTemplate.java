package com.lupur.api;

import com.lupur.business.dto.UserDTO;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Razvan Alexandru Lupu on 13-Jul-17.
 */

@Component("userRestTemplate")
public class UserRestTemplate {

    @Value("${rest.server.url}")
    private String REST_API_URL;

    @Value("${rest.server.username}")
    private String REST_USERNAME;

    @Value("${rest.server.password}")
    private String REST_PASSWORD;

    @Autowired
    private RestTemplate restTemplate;

    public UserDTO getUser(Integer id) throws RestClientException {
        HttpHeaders httpHeaders = new HttpHeaders();
        addAuthorizationHeader(httpHeaders);
        HttpEntity<String> request = new HttpEntity<>(httpHeaders);
        ResponseEntity<UserDTO> response = restTemplate.exchange(REST_API_URL+"/users/"+id,HttpMethod.GET,request,UserDTO.class);
        return response.getBody();
    }

    public List<UserDTO> getUsers() throws RestClientException {
        HttpHeaders httpHeaders = new HttpHeaders();
        addAuthorizationHeader(httpHeaders);
        HttpEntity<String> request = new HttpEntity<>(httpHeaders);
        ResponseEntity<UserDTO[]> response = restTemplate.exchange(REST_API_URL+"/users",HttpMethod.GET,request,UserDTO[].class);
        return Arrays.asList(response.getBody());
    }

    public List<UserDTO> getUsersByRole(String roleName) throws RestClientException {
        HttpHeaders httpHeaders = new HttpHeaders();
        addAuthorizationHeader(httpHeaders);
        HttpEntity<String> request = new HttpEntity<>(httpHeaders);
        ResponseEntity<UserDTO[]> response = restTemplate.exchange(REST_API_URL + "/users?role=" + roleName, HttpMethod.GET, request, UserDTO[].class);
        return Arrays.asList(response.getBody());
    }

    public UserDTO createUser(UserDTO userDTO) throws RestClientException{
        HttpHeaders httpHeaders = new HttpHeaders();
        addAuthorizationHeader(httpHeaders);
        HttpEntity<UserDTO> request = new HttpEntity<>(userDTO,httpHeaders);
        ResponseEntity<UserDTO> response = restTemplate.exchange(REST_API_URL + "/users", HttpMethod.POST, request, UserDTO.class);
        return response.getBody();
    }

    public UserDTO deleteUser(Integer id) throws RestClientException {
        HttpHeaders httpHeaders = new HttpHeaders();
        addAuthorizationHeader(httpHeaders);
        HttpEntity<String> request = new HttpEntity<>(httpHeaders);
        ResponseEntity<UserDTO> response = restTemplate.exchange(REST_API_URL+"/users/"+id,HttpMethod.DELETE,request,UserDTO.class);
        return response.getBody();

    }

    public UserDTO updateUser(Integer id, UserDTO userDTO) throws RestClientException{
        HttpHeaders httpHeaders = new HttpHeaders();
        addAuthorizationHeader(httpHeaders);
        HttpEntity<UserDTO> request = new HttpEntity<>(userDTO, httpHeaders);
        ResponseEntity<UserDTO> response = restTemplate.exchange(REST_API_URL+"/users/"+id,HttpMethod.PUT,request,UserDTO.class);
        return response.getBody();

    }

    private void addAuthorizationHeader(HttpHeaders httpHeaders){
        String plainCredentials = REST_USERNAME + ":" + REST_PASSWORD;
        String base64Credentials = Base64.encodeBase64String(plainCredentials.getBytes());
        httpHeaders.add("Authorization","Basic " + base64Credentials);
    }


}
