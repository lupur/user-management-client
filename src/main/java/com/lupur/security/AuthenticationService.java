package com.lupur.security;

import com.lupur.api.AuthenticationRestTemplate;
import com.lupur.business.dto.RoleDTO;
import com.lupur.business.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Razvan Alexandru Lupu on 17-Jul-17.
 */
@Service
public class AuthenticationService implements UserDetailsService {

    @Autowired
    private AuthenticationRestTemplate authenticationRestTemplate;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        try {
            UserDTO userDTO = authenticationRestTemplate.findByUsername(s);
            return new User(userDTO.getUsername(), userDTO.getPassword(), processAuthorities(userDTO.getRoles()));
        } catch (RestClientException ex) {
            throw new UsernameNotFoundException(s);
        }
    }

    private Set<GrantedAuthority> processAuthorities(Set<RoleDTO> roleDTOSet) {
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        for (RoleDTO roleDTO : roleDTOSet) {
            grantedAuthorities.add(new SimpleGrantedAuthority(roleDTO.getRoleName()));
        }
        return grantedAuthorities;
    }

}
