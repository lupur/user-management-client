package com.lupur.business.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Created by Razvan Alexandru Lupu on 17-Jul-17.
 */

public class RoleDTO {

    private Integer id;
    private String roleName;

    public RoleDTO() {
    }

    public RoleDTO(Integer id, String roleName) {
        this.id = id;
        this.roleName = roleName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Override
    public String toString() {
        return "{ID: " + id + ", Role name: " + roleName + "}";
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(this.id)
                .append(this.roleName)
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof RoleDTO)) return false;
        RoleDTO roleDTO = (RoleDTO) obj;
        return new EqualsBuilder()
                .append(this.id, roleDTO.getId())
                .append(this.roleName, roleDTO.getRoleName())
                .isEquals();
    }
}
