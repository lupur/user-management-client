package com.lupur.business.service;

import com.lupur.api.UserRestTemplate;
import com.lupur.business.dto.RoleDTO;
import com.lupur.business.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Created by Razvan Alexandru Lupu on 20-Jul-17.
 */


@Service
public class UserService {

    @Autowired
    private UserRestTemplate userRestTemplate;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public List<UserDTO> getUsers() {
        return userRestTemplate.getUsers();
    }

    public List<UserDTO> getUserByRole(String roleName) {
        return userRestTemplate.getUsersByRole(roleName);
    }

    public UserDTO getUser(Integer id) {
        return userRestTemplate.getUser(id);
    }

    public UserDTO createUser(UserDTO userDTO) {
        userDTO.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        userDTO.setRoles(addUserRole());
        return userRestTemplate.createUser(userDTO);
    }

    public UserDTO updateUser(Integer id, UserDTO userDTO) {
        UserDTO updateUserDTO = getUser(id);
        updateUserDTO.setFirstName(userDTO.getFirstName());
        updateUserDTO.setLastName(userDTO.getLastName());
        updateUserDTO.setUsername(userDTO.getUsername());
        updateUserDTO.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        updateUserDTO.setEmail(userDTO.getEmail());
        updateUserDTO.setDateOfBirth(userDTO.getDateOfBirth());
        updateUserDTO.setPhone(userDTO.getPhone());
        updateUserDTO.setAddress(userDTO.getAddress());
        return userRestTemplate.updateUser(id, updateUserDTO);
    }

    public UserDTO deleteUser(Integer id) {
        return userRestTemplate.deleteUser(id);
    }


    private Set<RoleDTO> addUserRole() {
        Set<RoleDTO> rolesDTO = new HashSet<>();
        rolesDTO.add(new RoleDTO(2, "USER"));
        return rolesDTO;
    }
}
