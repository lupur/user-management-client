package com.lupur;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserManagementClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserManagementClientApplication.class, args);
	}
}
