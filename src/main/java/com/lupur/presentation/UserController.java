package com.lupur.presentation;

import com.lupur.business.dto.UserDTO;
import com.lupur.business.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Razvan Alexandru Lupu on 14-Jul-17.
 */

@RequestMapping("/users")
@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @PreAuthorize("hasAnyAuthority('USER','ADMIN')")
    @GetMapping()
    public ModelAndView getUsers() {

        List<UserDTO> userDTOList = userService.getUsers();
        return new ModelAndView("user/user_list").addObject("userDTOList", userDTOList);

    }

    @PreAuthorize("hasAnyAuthority('USER','ADMIN')")
    @GetMapping(value = "", params = "role")
    public ModelAndView getUsersByRole(@PathParam("role") String role) {
        List<UserDTO> userDTOList = userService.getUserByRole(role);
        return new ModelAndView("user/user_list").addObject("userDTOList", userDTOList);

    }

    @PreAuthorize("hasAnyAuthority('USER','ADMIN')")
    @GetMapping("/{id}")
    public ModelAndView getUser(@PathVariable("id") Integer id) {
        List<UserDTO> userDTOList = new ArrayList<>();
        userDTOList.add(userService.getUser(id));
        return new ModelAndView("user/user_list").addObject("userDTOList", userDTOList);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("/create")
    public ModelAndView createUserGet() {
        return new ModelAndView("user/user_add").addObject("userDTO", new UserDTO());
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PostMapping("/create")
    public String createUserPost(@Valid @ModelAttribute("userDTO") UserDTO userDTO, Errors errors) {
        if(errors.hasErrors()){
            return "user/user_add";
        }
        UserDTO createdUserDTO = userService.createUser(userDTO);
        return "redirect:/users/"+createdUserDTO.getId();
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("/edit/{id}")
    public ModelAndView editUserGet(@PathVariable("id") Integer id) {
        UserDTO userDTO = userService.getUser(id);
        return new ModelAndView("user/user_edit").addObject("userDTO", userDTO);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PostMapping("/edit/{id}")
    public String editUserPost(@PathVariable("id") Integer id, @Valid @ModelAttribute("userDTO") UserDTO userDTO, Errors errors) {

        if(errors.hasErrors()){
            return "user/user_edit";
        }
        UserDTO updatedUserDTO = userService.updateUser(id, userDTO);
        return "redirect:/users/"+updatedUserDTO.getId();

    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @GetMapping("/delete/{id}")
    @PostMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") Integer id) {
        userService.deleteUser(id);
        return "redirect:/users";
    }

}
