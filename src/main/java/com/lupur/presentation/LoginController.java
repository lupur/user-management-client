package com.lupur.presentation;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Razvan Alexandru Lupu on 14-Jul-17.
 */

@Controller()
public class LoginController {

    @RequestMapping({"","/login.html","login"})
    public String login(){
        return "/login";
    }

    @RequestMapping("/login-error.html")
    public String loginWithError(Model model){
        model.addAttribute("loginError", true);
        return "/login.html";
    }

    @RequestMapping({"/index"})
    public String index(){
        return "/index";
    }
}
