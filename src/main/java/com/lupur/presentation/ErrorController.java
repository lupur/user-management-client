package com.lupur.presentation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lupur.api.ApiError;
import com.lupur.api.UserRestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.RestClientResponseException;

import java.io.IOException;


/**
 * Created by Razvan Alexandru Lupu on 14-Jul-17.
 */

@ControllerAdvice
public class ErrorController {

    private final Logger logger = LoggerFactory.getLogger(UserRestTemplate.class);

    @ExceptionHandler({BindException.class, Throwable.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String exceptionRestClient(final Throwable throwable, final Model model) {
        String errorMessage = (throwable != null ? throwable.getMessage() : "Unknown error");
        model.addAttribute("errorMessage", errorMessage);
        model.addAttribute("httpStatus", HttpStatus.INTERNAL_SERVER_ERROR);
        logger.error(errorMessage);
        return "error";
    }


    @ExceptionHandler(RestClientResponseException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String httpServerErrorException(final RestClientResponseException throwable, final Model model) throws IOException {
        String errorMessage = (throwable != null ? throwable.getMessage() : "Unknown error");
        ObjectMapper objectMapper = new ObjectMapper();
        ApiError apiError = objectMapper.readValue(throwable.getResponseBodyAsString(),ApiError.class);
        model.addAttribute("errorMessage", apiError.getMessage());
        model.addAttribute("errors", apiError.getErrors());
        model.addAttribute("httpStatus", apiError.getStatus());
        logger.error(apiError.getMessage());
        return "error";
    }

}
